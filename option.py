from optparse import OptionParser
def get_option():
    parser=OptionParser()
    parser.add_option("--local_rank", type=int, default=0)
    parser.add_option("--speaker_model", type=str, default="./prefile/model_raw.pkl", help="path for pretrained speaker model")
    parser.add_option("--channel", type=int, nargs='+', default=[32,32,32,32,32], help="channel for tranformer model")
    parser.add_option("--kernel_size", type=int ,nargs='+', default=[3,3,3,3,3], help="kernel size for transformer model")
    parser.add_option("--dilation", type=int, nargs='+', default=[1,2,5,2,1], help="dilation for transformer model")
    parser.add_option("--sample", type=int, nargs='+', default=[1,1,1,1,1], help="sample for transformer model")
    parser.add_option("--speaker_cfg", type=str, default="./prefile/timit_speaker.cfg", help="")
    (args,_)=parser.parse_args()
    return args