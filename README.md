# TIMIT-pytorch

#### 介绍
TIMIT说话人识别的，没有乱七八糟的功能，就一个功能，输入说话人返回说话人的id以及预测值，里面提供了两个说话人，完整的数据集放不下，去TIMIT官网下载吧

#### 安装教程

1.  pytorch1.6 最好有GPU 
2.  soundfile


#### 使用说明

1.  直接运行Sincnet.py就行
2.  文件路径看着稍微改改就行
3.  模型以及测试数据的链接：https://pan.baidu.com/s/1nJ1oZZsh9N_d4U43dRj2hw 提取码：ws7a 



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
